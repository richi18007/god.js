# God.js #

An overly cocky attempt to cover the common JS framework features into one framework. 

### What is this repository for? ###

This will include the following functionalities :-

1. A testing framework (working both on client and server)
2. AMD support.
3. Object Oriented JavaScript
4. Functional Javascript
5. Selector Engine
6. Events
7. Aliasing and Packing (Minification)
8. Shims 
9. Animations 
10. Touch Gesture support
11. Chained APIs

*TODO*: I don't know what else I could put in here. But all suggestions are welcome. 

## Detailed Framework Implementation ##

For the usage of any of the below methods, refer to the corresponding test suites 
For the test cases , I am currently using Jasmine - 2.0 

### Object Orientation ###

Using Douglas Crockford's paper on inheritance. It has a method called initialize which acts as a constructor.
The super methods are supported with the help of a private variable $super. 
Also , data members and mixins are supported.

### Functional Javascript ###

Functional javascript is supported with the following methods in place

1. each
2. map
3. filter
4. detect

In addition to all these , the chaining of all these methods is also supported

### CSS selector engine ###

TODO: Working on this currently 


### How do I get set up? ###

*TODO*: Include requireJS in your repository and include the required functionality .

### Contribution guidelines ###

*TODO*:I will fill up this section

### Who do I talk to? ###

Mail me at *richi18007@gmail.com* .
I am available on fb here : *facebook.com/richi18007*